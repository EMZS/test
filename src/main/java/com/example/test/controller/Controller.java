/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test.controller;

import com.example.test.dto.CompraDto;
import com.example.test.dto.OrdenDto;
import com.example.test.service.impl.OrdenServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @Autowired
    private OrdenServiceImpl ordenService;

    @PostMapping(path = "/agregarOrden")
    public ResponseEntity<Object> agregarOrden(@RequestBody CompraDto compra) {
        Long ordenId = ordenService.saveOrden(compra);
        return new ResponseEntity<Object>(ordenId, HttpStatus.OK);
    }

    @GetMapping(path = "/getOrden/{ordenId}")
    public ResponseEntity<Object> getOrden(@PathVariable Long ordenId) {
        System.out.println("ordenId:" + ordenId);

        OrdenDto ordenDto = ordenService.getOrden(ordenId);

        if (ordenDto == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(ordenService.getOrden(ordenId), HttpStatus.OK);
        }
    }
}
