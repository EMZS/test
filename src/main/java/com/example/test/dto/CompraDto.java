/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test.dto;

import java.util.List;

public class CompraDto {

    private Long sucursal_id;
    private List<ProductoDto> productos;

    public Long getSucursal_id() {
        return sucursal_id;
    }

    public void setSucursal_id(Long sucursal_id) {
        this.sucursal_id = sucursal_id;
    }

    public List<ProductoDto> getProductos() {
        return productos;
    }

    public void setProductos(List<ProductoDto> productos) {
        this.productos = productos;
    }

    @Override
    public String toString() {
        return "CompraDto{" + "sucursal_id=" + sucursal_id + ", productos=" + productos + '}';
    }

}
