/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test.repositories;

import com.example.test.entities.Orden;
import org.springframework.data.jpa.repository.JpaRepository;


public interface OrdenDao extends JpaRepository<Orden, Long>{
    
}
