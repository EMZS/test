/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test.repositories;

import com.example.test.entities.Sucursal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SucursalDao extends JpaRepository<Sucursal, Long>{
    
}
