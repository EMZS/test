/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test.service;

import com.example.test.dto.CompraDto;
import com.example.test.dto.OrdenDto;

public interface OrdenService {
    
    public Long saveOrden(CompraDto compraDto);
    
    public OrdenDto getOrden(Long orden_id);
}
