/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.test.service.impl;

import com.example.test.dto.CompraDto;
import com.example.test.dto.OrdenDto;
import com.example.test.dto.ProductoDto;
import com.example.test.entities.Orden;
import com.example.test.entities.Producto;
import com.example.test.entities.Sucursal;
import com.example.test.repositories.OrdenDao;
import com.example.test.repositories.ProductoDao;
import com.example.test.repositories.SucursalDao;
import com.example.test.service.OrdenService;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrdenServiceImpl implements OrdenService {

    @Autowired
    private OrdenDao ordenDao;

    @Autowired
    private SucursalDao sucursalDao;

    @Autowired
    private ProductoDao productoDao;

    @Override
    public Long saveOrden(CompraDto compraDto) {
        Sucursal sucursal = sucursalDao.findById(compraDto.getSucursal_id()).orElse(null);
        Orden orden = null;
        if (sucursal != null) {
            orden = new Orden();
            orden.setFecha(new Date());

            BigDecimal result = compraDto.getProductos().stream().map(ProductoDto::getPrecio).reduce(BigDecimal.ZERO, BigDecimal::add);
            orden.setTotal(result);
            orden.setSucursal(sucursal);
            ordenDao.save(orden);
            for (ProductoDto a : compraDto.getProductos()) {
                Producto p = new Producto();
                p.setOrden(orden);
                p.setCodigo(a.getCodigo());
                p.setDescripcion(a.getDescripcion());
                p.setPrecio(a.getPrecio());
                productoDao.save(p);
            };

        }

        return orden.getOrden_id() != null ? orden.getOrden_id() : null;
    }

    @Override
    public OrdenDto getOrden(Long orden_id) {
        Orden orden = ordenDao.findById(orden_id).orElse(null);
        OrdenDto o = null;
        if (orden != null) {
            o = new OrdenDto();
            o.setFecha(orden.getFecha());
            o.setOrden_id(orden.getOrden_id());
            o.setTotal(orden.getTotal());

        }
        return o;
    }

}
